var http = require('http');
var express = require('express');
var morgan = require('morgan');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var ejs = require('ejs');
var engine = require('ejs-mate');
var session = require('express-session');
var cookieParser = require('cookie-parser');
var flash = require('express-flash');
var MongoStore = require('connect-mongo/es5')(session);
var passport = require('passport');


var secret = require('./config/secret');
var User = require('./models/user');
var Category = require('./models/category');
var cartLength = require('./middlewares/middlewares');

var app = express();

mongoose.connect(secret.database, function(err){

  if(err){
    console.log(err);
  }else{
    console.log("Connected to the database!");
  }
});

//midleware
app.use(express.static(__dirname + '/public'));
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: secret.secretKey,
  store: new MongoStore({ url: secret.database, autoReconnect: true})
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
app.use(function(req, res, next) {
  res.locals.user = req.user;
  next();
});

app.use(cartLength);

app.use(function(req, res, next) {
  Category.find({}, function(err, categories){
    if(err) return next(err);
    res.locals.categories = categories;
    next();
  });
});


app.engine('ejs', engine);
app.set('view engine', 'ejs');


var mainRoutes = require('./routes/main');
var userRoutes = require('./routes/user');
var adminRoutes = require('./routes/admin');
var apiRoutes = require('./api/api');

app.use(mainRoutes);
app.use(userRoutes);
app.use(adminRoutes);
app.use('/api', apiRoutes);

app.get('/', function(req, res){
  res.render('main/home');
});

app.get('/about', function(req, res){
  res.render('main/about');
});
app.get('/contactus', function(req, res){
  res.render('partials/contactus');
});

// app.post('/contactus', function(req, res){
//   if(req.body.mail == "" || req.body.subject == ""){
//     res.send("Error: Email $ Subject should not be blank");
//     return false;
//   }


//   var smtpTransport = nodemailer.createTransport("SMTP", {
//     service: 'Gmail',
//     host: "smtp.gmail.com",
//     secureConnection: false,
//     port: 587,
//     auth:{
//       user: "quangbkhnk59@gmail.com",
//       pass: ""
//     }
//   });

//   var mailOptions = {
//     from: "quangbkhnk59@gmail.com",
//     to: req.body.email,
//     subject: req.body.subject+" -",
//     html: "<b>"+req.body.description+"<b>"
//   }
//   stmpTransport.sendMail(mailOptions, function(error, response){
//     if(error){
//       res.send("Email could not send:" + error);
//     }else{
//       res.send("Email has been sent success!")
//     }
//   });
// });


// app.post('/contactus', function(req, res){
//   var transport = nodemailer.createTransport({
//     service: 'Gmail',
//     author:{
//        user: 'techtest@gmail.com',
//        pass: ''
//     }
//   });
//   var mailOptions = {
//     from: ''
//   }
// });

app.listen(secret.port, function(err){
  if(err) throw err;
  console.log("Server is running on port: " + secret.port + ".");
});
